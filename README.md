# docker-letsencrypt
[![Docker pull](https://img.shields.io/docker/pulls/nouchka/letsencrypt)](https://hub.docker.com/r/nouchka/letsencrypt/)
[![Docker stars](https://img.shields.io/docker/stars/nouchka/letsencrypt)](https://hub.docker.com/r/nouchka/letsencrypt/)
[![Build Status](https://gitlab.com/japromis/docker-letsencrypt/badges/master/pipeline.svg)](https://gitlab.com/japromis/docker-letsencrypt/pipelines)
[![Docker size](https://img.shields.io/docker/image-size/nouchka/letsencrypt/latest)](https://hub.docker.com/r/nouchka/letsencrypt/)

# letsencrypt

# Usage (deprecated branch apache, docker tag latest to avoid breaking current)

Based on docker image lojzik/dockerfile-letsencrypt which provides certbot.
Check in docker-compose.yml for a working example, just complete haproxy conf file.
You have to specify a list of domains, certbot will generate files to validate domain.
Lighhtpd will provide theses files.
Haproxy will have a txt file with the list of ssl certificates to use.
Haproxy has to route traffic to lighttpd for url starting with /.well-known

# Usage (branch master, docker tag cloudflare)

cp env.template .env
change email, api token and domain
make run
Haproxy will have a txt file with the list of ssl certificates to use.

# Donate

Bitcoin Address: 15NVMBpZJTvkefwfsMAFA3YhyiJ5D2zd3R
